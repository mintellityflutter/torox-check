import 'package:flutter/cupertino.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';
import 'package:toroxcheck/widgets/base_step_page.dart';
import 'package:toroxcheck/widgets/col_divider.dart';

class CheckOnePage extends BaseStepPage {
  @override
  String getTitle() => 'Schritt 1';

  @override
  Widget getBody(BuildContext ctx) => Container(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 160,
              child: Placeholder(),
            ),
            ColDivider(),
            Text(ctx.translate('lorem_ipsum'))
          ],
        ),
      );

  @override
  NavButton getLeftButton(BuildContext ctx) => NavButton(ctx.translate('back'), () => {});

  @override
  NavButton getRightButton(BuildContext ctx) => NavButton(ctx.translate('next'), () => ctx.navigateTo(Routes.checkTwo));
}

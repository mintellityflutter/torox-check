import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';

class PasswordResettedPage extends StatelessWidget {
  PasswordResettedPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext ctx) => Scaffold(
      appBar: AppBar(
        title: Text(ctx.translate('password_resetted_title')),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(ctx.translate('password_resetted_head'), style: TextStyle(fontSize: 16)),
            const SizedBox(height: 60),
            Text(ctx.translate('password_resetted_body'), style: TextStyle(fontSize: 16))
          ],
        ),
      ));
}

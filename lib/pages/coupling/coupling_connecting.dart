import 'package:flutter/material.dart';
import 'package:toroxcheck/device/torox.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';
import 'package:toroxcheck/widgets/col_divider.dart';

class CouplingConnectingPage extends StatefulWidget {
  @override
  _CouplingConnectingPageState createState() => _CouplingConnectingPageState();
}

class _CouplingConnectingPageState extends State<CouplingConnectingPage> {
  var _torox = Torox();

  @override
  void initState() {
    super.initState();

    _torox.discover((devices) async {
      await _torox.connect(devices.first);
      context.navigateTo(Routes.selftest, clear: true);
    });
  }

  @override
  Widget build(BuildContext ctx) => Scaffold(
          body: Container(
        margin: MediaQuery.of(ctx).padding,
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Text(
              ctx.translate('connecting_torox_title'),
              style: ctx.theme().textTheme.headline5,
            ),
            Expanded(
                child: Center(
                    child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                    width: 80,
                    height: 80,
                    child:
                        CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(ctx.theme().primaryColor))),
                ColDivider(),
                Text(ctx.translate('please_wait'), style: TextStyle(fontSize: 16), textAlign: TextAlign.center),
              ],
            )))
          ],
        ),
      ));
}

import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';

class PrivacyPage extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) => Scaffold(
      appBar: AppBar(
        title: Text(ctx.translate('privacy')),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Text(ctx.translate('lorem_ipsum')),
      ));
}

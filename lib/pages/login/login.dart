import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:toroxcheck/auth/auth_handler.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';
import 'package:toroxcheck/widgets/primary_button.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final _authHandler = AuthHandler();
  final _mailController = TextEditingController();
  final _pswdController = TextEditingController();

  _onLogin() async {
    if (!_formKey.currentState.validate()) return;

    setState(() {
      _isLoading = true;
    });

    CognitoUserSession session;

    try {
      session = await _authHandler.authenticate(_mailController.text, _pswdController.text);
    } on CognitoUserNewPasswordRequiredException catch (e) {
      context.navigateTo(Routes.changePassword);
    } on CognitoClientException catch (e) {
      if (e.code == 'PasswordResetRequiredException')
        context.navigateTo(Routes.resetPassword);
      else
        context.showError(context.translate('invalid_credentials'));
    } catch (e) {
      context.showError(context.translate('login_failed'));
    }

    setState(() {
      _isLoading = false;
    });

    if (session != null) context.navigateTo(Routes.dashboard, clear: true);
  }

  _onResetPassword() => context.navigateTo(Routes.forgotPassword);

  @override
  void dispose() {
    _mailController.dispose();
    _pswdController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext ctx) => Scaffold(
      appBar: AppBar(
        title: Text(ctx.translate('app_name')),
      ),
      body: LoadingOverlay(
        isLoading: _isLoading,
        child: Container(
          padding: EdgeInsets.all(20),
          child: Form(
            key: _formKey,
            child: ListView(
              children: [
                Text(ctx.translate('login_head'), style: TextStyle(fontSize: 16)),
                const SizedBox(height: 60),
                TextFormField(
                  decoration: InputDecoration(labelText: ctx.translate('email')),
                  controller: _mailController,
                  validator: (v) => v.isEmpty ? ctx.translate('mail_required') : null,
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: ctx.translate('password')),
                  controller: _pswdController,
                  obscureText: true,
                  validator: (v) => v.isEmpty ? ctx.translate('pswd_required') : null,
                ),
                const SizedBox(height: 30),
                PrimaryButton(onPressed: _onLogin, text: ctx.translate('login')),
                const SizedBox(height: 30),
                FlatButton(
                  padding: EdgeInsets.all(16),
                  textColor: ctx.theme().primaryColor,
                  onPressed: _onResetPassword,
                  child: Text(ctx.translate('password_forgot'), style: TextStyle(fontSize: 16)),
                ),
              ],
            ),
          ),
        ),
      ));
}

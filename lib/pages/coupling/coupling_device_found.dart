import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';
import 'package:toroxcheck/widgets/base_step_page.dart';
import 'package:toroxcheck/widgets/col_divider.dart';

class CouplingDeviceFoundPage extends BaseStepPage {
  @override
  String getTitle() => null;

  @override
  Widget getBody(BuildContext ctx) => Container(
        padding: EdgeInsets.all(20),
        margin: MediaQuery.of(ctx).padding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _WifiIcon(),
            ColDivider(),
            ColDivider(),
            Text(
              ctx.translate('connect_device_found_title'),
              style: ctx.theme().textTheme.headline5,
            ),
            ColDivider(),
            ColDivider(),
            Text(ctx.translate('lorem_ipsum')),
            ColDivider(),
            ColDivider(),
          ],
        ),
        
      );

  @override
  NavButton getLeftButton(BuildContext ctx) => null;

  @override
  NavButton getRightButton(BuildContext ctx) =>
      NavButton(ctx.translate('next'), () => ctx.navigateTo(Routes.checkStart));
}

class _WifiIcon extends StatelessWidget {
  const _WifiIcon({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext ctx) => Center(
        child: Container(
          width: 100,
          height: 100,
          child: Center(
            child: Container(
              width: 80,
              height: 80,
              child: Center(
                child: Container(
                    width: 60,
                    height: 60,
                    child: Icon(
                      Icons.wifi,
                      size: 40,
                      color: Colors.white,
                    ),
                    decoration:
                        BoxDecoration(shape: BoxShape.circle, color: ctx.theme().primaryColor)),
              ),
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: ctx.theme().primaryColor.withAlpha(50)),
            ),
          ),
          decoration:
              BoxDecoration(shape: BoxShape.circle, color: ctx.theme().primaryColor.withAlpha(50)),
        ),
      );
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';

class SyncPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _SyncPageState();
}

class _SyncPageState extends State<SyncPage> {
  Timer _timer;

  _SyncPageState() {
    _timer = new Timer(const Duration(milliseconds: 2000), () {
      setState(() => _onSyncCompleted());
    });
  }

  _onSyncCompleted() => context.navigateTo(Routes.dashboard, clear: true);

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext ctx) => Scaffold(
      body: Container(
          padding: EdgeInsets.all(20),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(ctx.translate('sync_head'),
                    style: TextStyle(fontSize: 20), textAlign: TextAlign.center),
                SizedBox(height: 60),
                SizedBox(
                    width: 80,
                    height: 80,
                    child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(ctx.theme().primaryColor))),
                SizedBox(height: 60),
                Text(ctx.translate('please_wait'),
                    style: TextStyle(fontSize: 16), textAlign: TextAlign.center),
              ],
            ),
          )));
}

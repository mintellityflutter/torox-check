import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:toroxcheck/auth/auth_handler.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';
import 'package:toroxcheck/widgets/col_divider.dart';
import 'package:toroxcheck/widgets/primary_button.dart';

class PasswordForgotPage extends StatefulWidget {
  PasswordForgotPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _PasswordForgotPageState createState() => _PasswordForgotPageState();
}

class _PasswordForgotPageState extends State<PasswordForgotPage> {
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final _authHandler = AuthHandler();
  final _mailController = TextEditingController();

  _onPasswordReset() async {
    if (!_formKey.currentState.validate()) return;

    setState(() {
      _isLoading = true;
    });

    await _authHandler.forgotPassword(_mailController.text);
    context.navigateTo(Routes.resetPassword);
    context.showSuccess(context.translate('send_confirmation_code_success'));

    setState(() {
      _isLoading = false;
    });
  }

  _onCodeAvailable() => context.navigateTo(Routes.resetPassword);

  String _validateMail(String v) => v.isEmpty ? context.translate('input_required') : null;

  @override
  Widget build(BuildContext ctx) => Scaffold(
      appBar: AppBar(
        title: Text(ctx.translate('password_reset_title')),
      ),
      body: LoadingOverlay(
        isLoading: _isLoading,
        child: Container(
          padding: EdgeInsets.all(20),
          child: Form(
            key: _formKey,
            child: ListView(
              children: [
                Text(ctx.translate('password_reset_head'), style: TextStyle(fontSize: 16)),
                const SizedBox(height: 60),
                TextFormField(
                  validator: _validateMail,
                  decoration: InputDecoration(labelText: ctx.translate('email')),
                  controller: _mailController,
                ),
                const SizedBox(height: 30),
                PrimaryButton(onPressed: _onPasswordReset, text: ctx.translate('password_reset_button')),
                ColDivider(),
                FlatButton(
                  padding: EdgeInsets.all(16),
                  textColor: ctx.theme().primaryColor,
                  onPressed: _onCodeAvailable,
                  child: Text(ctx.translate('verification_code_already_received'), style: TextStyle(fontSize: 16)),
                ),
              ],
            ),
          ),
        ),
      ));
}

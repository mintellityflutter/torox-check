import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/widgets/col_divider.dart';

class CheckStepsListPage extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) => Scaffold(
      appBar: AppBar(
        title: Text(ctx.translate('check_steps_title')),
      ),
      body: Container(
          padding: EdgeInsets.all(20),
          child: GridView.count(
            crossAxisCount: 2,
            children: List.generate(4, (index) => _CheckStepListItem('Schritt $index', () => {})),
          )));
}

class _CheckStepListItem extends StatelessWidget {
  final String title;
  final GestureTapCallback callback;

  _CheckStepListItem(this.title, this.callback);

  @override
  Widget build(BuildContext ctx) => Card(
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(title, style: ctx.theme().textTheme.headline6),
              ColDivider(),
              AspectRatio(
                aspectRatio: 1.4,
                child: Placeholder(),
              )
            ],
          ),
        ),
      );
}

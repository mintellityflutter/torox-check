import 'dart:async';
import 'dart:convert';

import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:toroxcheck/extensions/environment.dart';

class Torox {
  static const _scanDuration = Duration(seconds: 2);
  static final Guid _serviceId = Guid('0000ffe0-0000-1000-8000-00805f9b34fb');
  static final Torox _singleton = Torox._internal();

  BluetoothDevice _device;
  Completer<String> _responseCompleter;
  List<BluetoothDevice> _discovered = List();
  List<BluetoothService> _services;
  BluetoothCharacteristic _characteristic;

  factory Torox() => _singleton;

  Torox._internal();

  Future<String> send(String value) async {
    print('# >> $value');
    _characteristic?.write(utf8.encode(value));

    _responseCompleter = Completer();

    return _responseCompleter.future;
  }

  connect(BluetoothDevice device) async {
    _device?.disconnect();
    _device = device;

    return _device
        .connect()
        .then((value) => _initServices())
        .then((value) => _initCharacteristic())
        .then((value) => send(DotEnv().toroxConnKey));
  }

  discover(void onDone(List<BluetoothDevice> devices)) {
    _discovered.clear();

    var fblue = FlutterBlue.instance;

    fblue.startScan(timeout: _scanDuration);

    fblue.scanResults.listen((results) {
      results.map((e) => e.device).where((d) => d.name.startsWith('un_')).toList().forEach((f) {
        if (!_discovered.contains(f)) {
          _discovered.add(f);
        }
      });
    });

    fblue.stopScan();

    // TODO: workaround for the moment, try with subscription
    new Timer(_scanDuration, () => onDone(_discovered));
  }

  disconnect() => _device?.disconnect();

  _initServices() async {
    _services = await _device?.discoverServices();
  }

  _initCharacteristic() async {
    _characteristic = _services?.firstWhere((element) => element.uuid == _serviceId)?.characteristics?.first;

    await _characteristic?.setNotifyValue(true);
    _characteristic.value.listen(_onResponse);
  }

  _onResponse(event) {
    var str = utf8.decode(event).replaceAll('\n', '');
    print('# << $str');

    _responseCompleter?.complete(str);
  }
}

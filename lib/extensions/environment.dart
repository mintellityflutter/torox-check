import 'package:flutter_dotenv/flutter_dotenv.dart';

extension EnvironmentExt on DotEnv {
  String get toroxConnKey => this.env['TOROX_CONN_KEY'];
  String get cognitoClientId => this.env['COGNITO_CLIENT_ID'];
  String get cognitoUserPoolId => this.env['COGNITO_USER_POOL_ID'];
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:yaml/yaml.dart';

class AppLocalizations {
  final Locale locale;
  Map<String, String> _map;

  AppLocalizations(this.locale);

  static const LocalizationsDelegate<AppLocalizations> delegate = _AppLocalizationsDelegate();

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  Future load() async {
    String yamlStr = await rootBundle.loadString('lang/${locale.languageCode}.yaml');

    Map<dynamic, dynamic> yamlMap = loadYaml(yamlStr).map((key, val) => MapEntry(key, val));
    _map = yamlMap.map((key, val) => MapEntry(key.toString(), val.toString()));
  }

  String translate(String key) => _map[key];
}

class _AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ['de'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) async {
    var localizations = new AppLocalizations(locale);
    await localizations.load();

    return localizations;
  }

  @override
  bool shouldReload(covariant LocalizationsDelegate<AppLocalizations> old) => false;
}

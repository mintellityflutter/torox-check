import 'dart:async';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:toroxcheck/device/commands.dart';
import 'package:toroxcheck/device/torox.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';
import 'package:toroxcheck/widgets/col_divider.dart';

class SelftestPage extends StatefulWidget {
  @override
  _SelftestPageState createState() => _SelftestPageState();
}

class _SelftestPageState extends State<SelftestPage> {
  var _torox = Torox();
  var _testing = false;
  int _testCounter = 0;
  List<Function> _tests;

  var _bodyKeys = [
    'selftest_body_bi',
    'selftest_body_dc',
    'selftest_body_ac',
    'selftest_body_hv',
    'selftest_body_hn',
  ];

  var _headerKeys = [
    'selftest_header_bi',
    'selftest_header_dc',
    'selftest_header_ac',
    'selftest_header_hv',
    'selftest_header_hn',
  ];

  _test() async {
    setState(() => {_testing = true});

    await _tests[_testCounter].call();

    if (++_testCounter == _tests.length) {
      context.navigateTo(Routes.dashboard, clear: true);

      Flushbar(
        margin: EdgeInsets.all(16),
        message: context.translate('selftest_success'),
        duration: Duration(seconds: 4),
        borderRadius: 8,
        icon: Icon(
          Icons.check,
          size: 28.0,
          color: Colors.green[300],
        ),
        leftBarIndicatorColor: Colors.green[300],
      )..show(context);

      return;
    }

    setState(() => {_testing = false});
  }

  _testAC() async {
    for (var i = 0; i < 3; i++) await _torox.send(Commands.ac);
  }

  _testDC() async {
    for (var i = 0; i < 3; i++) await _torox.send(Commands.dc);
  }

  _testBI() async {
    await _torox.send(Commands.bi);
  }

  _testHV() async {
    await _torox.send(Commands.hp);
    await Future.delayed(Duration(seconds: 3), () => _torox.send(Commands.hn));
  }

  _testHN() async {
    await _torox.send(Commands.hn);
  }

  @override
  void initState() {
    super.initState();

    _tests = [_testBI, _testDC, _testAC, _testHV, _testHN];
  }

  @override
  Widget build(BuildContext ctx) => Scaffold(
        appBar: AppBar(
          title: Text(ctx.translate('selftest_title')),
        ),
        body: LoadingOverlay(
          isLoading: _testing,
          child: Container(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  LinearProgressIndicator(value: 1 / _tests.length * (_testCounter + 1)),
                  ColDivider(),
                  Text(ctx.translate(_headerKeys[_testCounter]), style: ctx.theme().textTheme.headline4),
                  ColDivider(),
                  Text(ctx.translate(_bodyKeys[_testCounter]), style: ctx.theme().textTheme.bodyText2),
                ],
              )),
        ),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.play_arrow),
            onPressed: _testing ? null : () => _test(),
            backgroundColor: _testing ? Colors.grey[300] : Colors.green[300]),
      );
}

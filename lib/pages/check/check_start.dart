import 'package:flutter/cupertino.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';
import 'package:toroxcheck/widgets/base_step_page.dart';
import 'package:toroxcheck/widgets/col_divider.dart';

class CheckStartPage extends BaseStepPage {
  @override
  String getTitle() => 'Test';

  @override
  Widget getBody(BuildContext ctx) => Container(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 160,
              child: Placeholder(),
            ),
            ColDivider(),
            Text(
              ctx.translate('start_check_for_tester'),
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
            ),
            ColDivider(),
            Text(ctx.translate('lorem_ipsum'))
          ],
        ),
      );

  @override
  NavButton getLeftButton(BuildContext ctx) => NavButton(ctx.translate('back'), () => {});

  @override
  NavButton getRightButton(BuildContext ctx) => NavButton(ctx.translate('next'), () => ctx.navigateTo(Routes.checkOne));
}

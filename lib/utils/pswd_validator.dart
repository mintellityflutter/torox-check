class PasswordValidator {
  static List<PasswordErrors> validate(String pswd) {
    pswd = pswd ?? "";
    var result = List<PasswordErrors>();

    if (pswd.length < 8) result.add(PasswordErrors.tooShort);
    if (!pswd.contains(RegExp(r'[A-Z]'))) result.add(PasswordErrors.noUppercase);
    if (!pswd.contains(RegExp(r'[a-z]'))) result.add(PasswordErrors.noLowercase);
    if (!pswd.contains(RegExp(r'[0-9]'))) result.add(PasswordErrors.noDigits);

    return result;
  }
}

enum PasswordErrors {
  tooShort,
  noDigits,
  noUppercase,
  noLowercase,
}

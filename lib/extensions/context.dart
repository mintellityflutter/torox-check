import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:toroxcheck/utils/app_localizations.dart';

extension ContextHelpers on BuildContext {
  navigateTo(String route, {bool clear = false, bool pop = false}) {
    if (clear)
      Navigator.pushNamedAndRemoveUntil(this, route, (r) => false);
    else {
      if (pop)
        Navigator.popAndPushNamed(this, route);
      else
        Navigator.pushNamed(this, route);
    }
  }

  String translate(String key) => AppLocalizations.of(this).translate(key);

  ThemeData theme() => Theme.of(this);

  showInfo(msg) => showFlushbar(msg, icon: Icons.info, color: Colors.orange[400]);

  showError(msg) => showFlushbar(msg, icon: Icons.error, color: Colors.red[400]);

  showSuccess(msg) => showFlushbar(msg, icon: Icons.check, color: Colors.green[400]);

  showFlushbar(msg, {IconData icon, Color color, Duration duration = const Duration(seconds: 2)}) => Flushbar(
        margin: EdgeInsets.all(16),
        message: msg,
        duration: duration,
        icon: Icon(
          icon,
          size: 28.0,
          color: color,
        ),
        leftBarIndicatorColor: color,
      )..show(this);
}

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';
import 'package:toroxcheck/widgets/base_step_page.dart';
import 'package:toroxcheck/widgets/col_divider.dart';

class CouplingActivateBluetoothPage extends BaseStepPage {
  Timer _timer;
  BuildContext _ctx;

  CouplingActivateBluetoothPage() : super() {
    _timer = new Timer(const Duration(milliseconds: 2000), () {
      _timer.cancel();
      _ctx?.navigateTo(Routes.couplingConnecting);
    });
  }

  @override
  String getTitle() => null;

  @override
  Widget getBody(BuildContext ctx) {
    _ctx = ctx;

    return Container(
      padding: EdgeInsets.all(20),
      margin: MediaQuery.of(ctx).padding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            ctx.translate('activate_bluetooth_title'),
            style: ctx.theme().textTheme.headline5,
          ),
          ColDivider(),
          ColDivider(),
          Text(ctx.translate('activate_bluetooth_line_1')),
          ColDivider(),
          Text(ctx.translate('activate_bluetooth_line_2')),
          ColDivider(),
          Text(ctx.translate('activate_bluetooth_line_3')),
        ],
      ),
    );
  }

  @override
  NavButton getLeftButton(BuildContext ctx) =>
      NavButton(ctx.translate('back'), () => ctx.navigateTo(Routes.couplingStart, pop: true));

  @override
  NavButton getRightButton(BuildContext ctx) => null;
}

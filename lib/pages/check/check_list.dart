import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';

class CheckListPage extends StatelessWidget {
  _onTap(BuildContext ctx) => ctx.navigateTo(Routes.checkDevice);

  @override
  Widget build(BuildContext ctx) => Scaffold(
      appBar: AppBar(
        title: Text(ctx.translate('check_list_title')),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          _CheckListItem(
              'Monitor\nKategorie', 'Status: in Bearbeitung (10.10.2020 um 13:37)', 'asd', () => _onTap(ctx)),
          _CheckListItem(
              'Netzteil\nKategorie', 'Status: in Bearbeitung (10.10.2020 um 13:37)', 'asd', () => _onTap(ctx)),
          _CheckListItem(
              'Staubsauger\nKategorie', 'Status: in Bearbeitung (10.10.2020 um 13:37)', 'asd', () => _onTap(ctx)),
          _CheckListItem(
              'Fernseher\nKategorie', 'Status: abgeschlossen  (10.10.2020 um 20:20)', 'asd', () => _onTap(ctx)),
        ]),
      ));
}

class _CheckListItem extends StatelessWidget {
  final String title;
  final String state;
  final String category;
  final GestureTapCallback callback;

  _CheckListItem(this.title, this.state, this.category, this.callback);

  @override
  Widget build(BuildContext context) => Card(
        child: ListTile(
          onTap: callback,
          title: Text(title),
          subtitle: Text(state),
          trailing: Icon(Icons.chevron_right),
        ),
      );
}

import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';

class DashboardOrdersPage extends StatelessWidget {
  _onTap(BuildContext ctx) => ctx.navigateTo(Routes.checkList);

  @override
  Widget build(BuildContext ctx) => Scaffold(
          body: Container(
        child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          _OrderItem('Lorem Ipsum', 'Status: in Bearbeitung (10.10.2020 um 13:37)', () => _onTap(ctx)),
          _OrderItem('Lorem Ipsum', 'Status: in Bearbeitung (10.10.2020 um 13:37)', () => _onTap(ctx)),
          _OrderItem('Lorem Ipsum', 'Status: in Bearbeitung (10.10.2020 um 13:37)', () => _onTap(ctx)),
          _OrderItem('Lorem Ipsum', 'Status: abgeschlossen  (10.10.2020 um 20:20)', () => _onTap(ctx)),
        ]),
      ));
}

class _OrderItem extends StatelessWidget {
  final String title;
  final String state;
  final GestureTapCallback callback;

  _OrderItem(this.title, this.state, this.callback);

  @override
  Widget build(BuildContext context) => Card(
        child: ListTile(
          onTap: callback,
          title: Text(title),
          subtitle: Text(state),
          trailing: Icon(Icons.chevron_right),
        ),
      );
}

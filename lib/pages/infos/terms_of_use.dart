import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';

class TermsOfUsePage extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) => Scaffold(
      appBar: AppBar(
        title: Text(ctx.translate('terms_of_use')),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Text(ctx.translate('lorem_ipsum')),
      ));
}

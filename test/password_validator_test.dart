import 'package:flutter_test/flutter_test.dart';

import 'package:toroxcheck/utils/pswd_validator.dart';

void main() {
  test('Empty input', () {
    var errors = PasswordValidator.validate("");

    expect(errors.length, 4);
  });

  test('Null input', () {
    var errors = PasswordValidator.validate(null);

    expect(errors.length, 4);
  });

  test('Uppercase', () {
    var errors = PasswordValidator.validate("U");

    expect(errors.length, 3);
    expect(errors.contains(PasswordErrors.noUppercase), false);
  });

  test('Lowercase', () {
    var errors = PasswordValidator.validate("u");

    expect(errors.length, 3);
    expect(errors.contains(PasswordErrors.noLowercase), false);
  });

  test('Digits', () {
    var errors = PasswordValidator.validate("1");

    expect(errors.length, 3);
    expect(errors.contains(PasswordErrors.noDigits), false);
  });

  test('Length', () {
    var errors = PasswordValidator.validate("12345678");

    expect(errors.length, 2);
    expect(errors.contains(PasswordErrors.tooShort), false);
  });

  test('Success', () {
    var errors = PasswordValidator.validate("Test2222");

    expect(errors.length, 0);
  });
}

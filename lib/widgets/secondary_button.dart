import 'package:flutter/material.dart';

class SecondaryButton extends RaisedButton {
  final String text;
  final VoidCallback onPressed;

  SecondaryButton({@required this.onPressed, @required this.text})
      : super(
            child: Text(text, style: TextStyle(fontSize: 20)),
            color: Colors.blue.shade200,
            padding: EdgeInsets.all(16),
            onPressed: onPressed,
            textColor: Colors.white);
}

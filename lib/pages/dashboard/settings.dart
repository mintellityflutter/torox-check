import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/widgets/col_divider.dart';
import 'package:toroxcheck/widgets/primary_button.dart';

class SettingsPage extends StatelessWidget {
  _onSave() {}

  @override
  Widget build(BuildContext ctx) => Scaffold(
          body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextFormField(decoration: InputDecoration(labelText: ctx.translate('name'))),
            TextFormField(decoration: InputDecoration(labelText: ctx.translate('phone'))),
            TextFormField(obscureText: true, decoration: InputDecoration(labelText: ctx.translate('password'))),
            TextFormField(obscureText: true, decoration: InputDecoration(labelText: ctx.translate('repeat_password'))),
            ColDivider(),
            PrimaryButton(onPressed: _onSave, text: ctx.translate('save_settings')),
          ],
        ),
      ));
}

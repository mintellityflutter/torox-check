import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';

abstract class BaseStepPage extends StatelessWidget {
  String _title;
  TextStyle _bottomTextStyle = TextStyle(color: Colors.white, fontSize: 16);

  BaseStepPage() {
    _title = getTitle();
  }

  @protected
  Widget getBody(BuildContext ctx);

  @protected
  String getTitle();

  @protected
  NavButton getLeftButton(BuildContext ctx);

  @protected
  NavButton getRightButton(BuildContext ctx);

  AppBar _getAppBar() => _title == null
      ? null
      : AppBar(
          title: Text(_title),
        );

  FlatButton _createNavButton(_NavButtonType type, NavButton cfg) {
    if (cfg == null) return FlatButton(onPressed: null, child: null);

    var _text = Text(
      cfg.text,
      style: _bottomTextStyle,
    );

    var _icon = Icon(
      type == _NavButtonType.left ? Icons.chevron_left : Icons.chevron_right,
      color: Colors.white,
    );

    return FlatButton(
      onPressed: cfg.callback,
      child: Row(
        children: type == _NavButtonType.left ? [_icon, _text] : [_text, _icon],
      ),
    );
  }

  @override
  Widget build(BuildContext ctx) => Scaffold(
      appBar: _getAppBar(),
      body: getBody(ctx),
      bottomNavigationBar: SizedBox(
        height: 60,
        child: BottomAppBar(
          color: ctx.theme().primaryColor,
          child: Container(
            child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              _createNavButton(_NavButtonType.left, getLeftButton(ctx)),
              _createNavButton(_NavButtonType.right, getRightButton(ctx)),
            ]),
          ),
        ),
      ));
}

enum _NavButtonType { left, right }

class NavButton {
  String text;
  VoidCallback callback;

  NavButton(this.text, this.callback);
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';
import 'package:toroxcheck/widgets/base_step_page.dart';
import 'package:toroxcheck/widgets/col_divider.dart';
import 'package:toroxcheck/widgets/primary_button.dart';

class CouplingDeviceNotFoundPage extends BaseStepPage {
  @override
  String getTitle() => null;

  @override
  Widget getBody(BuildContext ctx) => Container(
        padding: EdgeInsets.all(20),
        margin: MediaQuery.of(ctx).padding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              ctx.translate('connect_device_not_found_title'),
              style: ctx.theme().textTheme.headline5,
            ),
            ColDivider(),
            ColDivider(),
            Text(ctx.translate('connect_device_not_found_body')),
            ColDivider(),
            ColDivider(),
            SizedBox(
              width: double.infinity,
              child:
                  PrimaryButton(onPressed: () => {}, text: ctx.translate('open_system_settings')),
            ),
          ],
        ),
      );

  @override
  NavButton getLeftButton(BuildContext ctx) =>
      NavButton(ctx.translate('back'), () => ctx.navigateTo(Routes.couplingStart, pop: true));

  @override
  NavButton getRightButton(BuildContext ctx) => null;
}

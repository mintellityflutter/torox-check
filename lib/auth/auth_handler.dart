import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:toroxcheck/extensions/environment.dart';

class AuthHandler {
  final _userPool = CognitoUserPool(DotEnv().cognitoUserPoolId, DotEnv().cognitoClientId);
  static final AuthHandler _singleton = AuthHandler._internal();

  CognitoUser _user;

  factory AuthHandler() => _singleton;

  AuthHandler._internal();

  Future<CognitoUserSession> authenticate(String mail, String pswd) {
    _user = CognitoUser(mail, _userPool);

    return _user.authenticateUser(AuthenticationDetails(username: mail, password: pswd));
  }

  Future<CognitoUserSession> changePassword(String pswd) {
    if (_user == null) throw Exception('no user provided: authenticate() before');

    return _user.sendNewPasswordRequiredAnswer(pswd);
  }

  Future forgotPassword(String mail) {
    _user = CognitoUser(mail, _userPool);

    return _user.forgotPassword();
  }

  Future<bool> confirmPassword(String mail, String pswd, String code) {
    _user = CognitoUser(mail, _userPool);

    return _user.confirmPassword(code, pswd);
  }
}

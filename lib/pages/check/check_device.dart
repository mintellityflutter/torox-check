import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';
import 'package:toroxcheck/widgets/col_divider.dart';
import 'package:toroxcheck/widgets/primary_button.dart';
import 'package:toroxcheck/widgets/secondary_button.dart';

class CheckDevicePage extends StatelessWidget {
  _onTapChecks(BuildContext ctx) => ctx.navigateTo(Routes.checkList);
  _onTapCheckSteps(BuildContext ctx) => ctx.navigateTo(Routes.checkSteps);

  @override
  Widget build(BuildContext ctx) => Scaffold(
      appBar: AppBar(
        title: Text(ctx.translate('check_device_title')),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        child: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          SizedBox(height: 200, child: Placeholder()),
          ColDivider(),
          TextFormField(
              initialValue: 'Test',
              readOnly: true,
              decoration: InputDecoration(labelText: ctx.translate('device_name'))),
          TextFormField(
              initialValue: 'Test', readOnly: true, decoration: InputDecoration(labelText: ctx.translate('category'))),
          TextFormField(
              initialValue: 'Test',
              readOnly: true,
              decoration: InputDecoration(labelText: ctx.translate('inventory_nr'))),
          TextFormField(
              initialValue: '4',
              readOnly: true,
              onTap: () => _onTapChecks(ctx),
              decoration: InputDecoration(labelText: ctx.translate('checks'), suffixIcon: Icon(Icons.chevron_right))),
          ColDivider(),
          SecondaryButton(onPressed: () => _onTapCheckSteps(ctx), text: ctx.translate('show_check_steps')),
          ColDivider(),
          PrimaryButton(onPressed: () => {}, text: ctx.translate('start_new_check')),
        ]),
      ));
}

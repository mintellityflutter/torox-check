import 'package:flutter/material.dart';

class PrimaryButton extends RaisedButton {
  final String text;
  final VoidCallback onPressed;

  PrimaryButton({@required this.onPressed, @required this.text})
      : super(
            child: Text(text, style: TextStyle(fontSize: 20)),
            padding: EdgeInsets.all(16),
            onPressed: onPressed,
            textColor: Colors.white);
}

import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/pages/dashboard/index.dart';
import 'package:toroxcheck/pages/dashboard/orders.dart';
import 'package:toroxcheck/pages/dashboard/settings.dart';
import 'package:toroxcheck/routes/routes.dart';

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  String _titleKey = 'dashboard';
  Widget _bodyWidget = DashboardIndexPage();

  _setBody(Widget widget) {
    setState(() {
      _bodyWidget = widget;
      Navigator.of(context).pop();
    });
  }

  _setTitle(String key) => _titleKey = key;

  @override
  Widget build(BuildContext ctx) => Scaffold(
      appBar: AppBar(
        title: Text(ctx.translate(_titleKey)),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 94,
              child: DrawerHeader(
                decoration: BoxDecoration(
                  color: ctx.theme().primaryColor,
                ),
                child: Text(
                  ctx.translate('app_name'),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                  ),
                ),
              ),
            ),
            ListTile(
                title: Text(ctx.translate('dashboard')),
                leading: Icon(Icons.home),
                onTap: () => {_setTitle('dashboard'), _setBody(DashboardIndexPage())}),
            ListTile(
              title: Text(ctx.translate('test_orders')),
              leading: Icon(Icons.list),
              onTap: () => {_setTitle('test_orders'), _setBody(DashboardOrdersPage())},
            ),
            ListTile(
              title: Text(ctx.translate('app_name')),
              leading: Icon(Icons.devices),
            ),
            ListTile(
              title: Text(ctx.translate('privacy')),
              leading: Icon(Icons.info),
              onTap: () => ctx.navigateTo(Routes.privacy, pop: true),
            ),
            ListTile(
              title: Text(ctx.translate('imprint')),
              leading: Icon(Icons.info),
              onTap: () => ctx.navigateTo(Routes.imprint, pop: true),
            ),
            ListTile(
              title: Text(ctx.translate('terms_of_use')),
              leading: Icon(Icons.info),
              onTap: () => ctx.navigateTo(Routes.termsOfUse, pop: true),
            ),
            ListTile(
              title: Text(ctx.translate('settings')),
              leading: Icon(Icons.settings),
              onTap: () => {_setTitle('settings'), _setBody(SettingsPage())},
            ),
            ListTile(
              title: Text(ctx.translate('logout')),
              leading: Icon(Icons.logout),
              onTap: () => ctx.navigateTo(Routes.login, clear: true),
            )
          ],
        ),
      ),
      body: Container(padding: EdgeInsets.all(20), child: _bodyWidget));
}

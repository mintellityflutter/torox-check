import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';
import 'package:toroxcheck/widgets/base_step_page.dart';
import 'package:toroxcheck/widgets/col_divider.dart';

class CouplingStartPage extends BaseStepPage {
  @override
  String getTitle() => null;

  @override
  Widget getBody(BuildContext ctx) => Container(
        padding: EdgeInsets.all(20),
        margin: MediaQuery.of(ctx).padding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              ctx.translate('connect_torox_title'),
              style: ctx.theme().textTheme.headline5,
            ),
            ColDivider(),
            ColDivider(),
            Text(ctx.translate('connect_torox_line_1')),
            ColDivider(),
            Text(ctx.translate('connect_torox_line_2')),
            ColDivider(),
            Text(ctx.translate('connect_torox_line_3')),
          ],
        ),
      );

  @override
  NavButton getLeftButton(BuildContext ctx) => NavButton(ctx.translate('back'), () => {});

  @override
  NavButton getRightButton(BuildContext ctx) =>
      NavButton(ctx.translate('next'), () => ctx.navigateTo(Routes.couplingActivateBluetooth));
}

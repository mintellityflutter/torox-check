import 'package:flutter/material.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/widgets/col_divider.dart';
import 'package:toroxcheck/widgets/primary_button.dart';

class DashboardIndexPage extends StatelessWidget {
  _onSync() {}

  @override
  Widget build(BuildContext ctx) => Scaffold(
          body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(height: 200, child: Placeholder()),
            ColDivider(),
            Text(ctx.translate('lorem_ipsum')),
            ColDivider(),
            PrimaryButton(onPressed: _onSync, text: ctx.translate('sync_data')),
          ],
        ),
      ));
}

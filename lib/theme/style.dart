import 'package:flutter/material.dart';

ThemeData appTheme() => ThemeData(
    primarySwatch: Colors.blue,
    visualDensity: VisualDensity.adaptivePlatformDensity,
    textTheme: TextTheme(
      bodyText2: TextStyle(fontSize: 16)
    ),
    buttonColor: Colors.blue);

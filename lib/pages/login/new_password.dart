import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:toroxcheck/auth/auth_handler.dart';
import 'package:toroxcheck/extensions/context.dart';
import 'package:toroxcheck/routes/routes.dart';
import 'package:toroxcheck/utils/pswd_validator.dart';
import 'package:toroxcheck/widgets/col_divider.dart';
import 'package:toroxcheck/widgets/primary_button.dart';

class NewPasswordPage extends StatefulWidget {
  final NewPasswordMode _mode;

  NewPasswordPage(this._mode, {Key key}) : super(key: key);

  @override
  _NewPasswordPageState createState() => _NewPasswordPageState(_mode);
}

class _NewPasswordPageState extends State<NewPasswordPage> {
  final _formKey = GlobalKey<FormState>();
  final _authHandler = AuthHandler();
  final NewPasswordMode _mode;
  final _mailController = TextEditingController();
  final _pswdController = TextEditingController();
  final _codeController = TextEditingController();
  final _pswdRepeatController = TextEditingController();

  bool _isLoading = false;
  bool _pswdHasDigits = false;
  bool _pswdHasLength = false;
  bool _pswdHasUppercase = false;
  bool _pswdHasLowercase = false;
  bool _pswdReqFulfilled = false;

  _NewPasswordPageState(this._mode);

  _onNewPassword() async {
    if (!_formKey.currentState.validate()) return;

    setState(() {
      _isLoading = true;
    });

    try {
      if (_mode == NewPasswordMode.change) {
        var session = await _authHandler.changePassword(_pswdController.text);
        if (session != null) {
          context.navigateTo(Routes.dashboard, clear: true);
          context.showSuccess(context.translate('new_password_success'));
        }
      } else {
        if (await _authHandler.confirmPassword(_mailController.text, _pswdController.text, _codeController.text)) {
          context.navigateTo(Routes.login, clear: true);
          context.showSuccess(context.translate('new_password_success'));
        }
      }
    } on CognitoClientException catch (e) {
      switch (e.code) {
        case 'ExpiredCodeException':
          {
            context.showError(context.translate('confirmation_code_expired'));
          }
          break;
        default:
          {
            context.showError(context.translate('new_password_failed'));
          }
      }
    }

    setState(() {
      _isLoading = false;
    });
  }

  String _validatePswd(String v) {
    if (v.isEmpty) return context.translate('input_required');
    if (!_pswdReqFulfilled) return context.translate('pswd_req_not_fulfilled');

    return null;
  }

  String _validatePswdRepeat(String v) {
    if (v.isEmpty) return context.translate('input_required');
    if (_pswdController.text != _pswdRepeatController.text) return context.translate('pswd_repeat_not_match');

    return null;
  }

  String _validateVerificationCode(String v) => v.isEmpty ? context.translate('input_required') : null;

  void _onPasswordChanged(String val) {
    var errors = PasswordValidator.validate(val);

    setState(() {
      _pswdHasLength = !errors.contains(PasswordErrors.tooShort);
      _pswdHasDigits = !errors.contains(PasswordErrors.noDigits);
      _pswdHasLowercase = !errors.contains(PasswordErrors.noLowercase);
      _pswdHasUppercase = !errors.contains(PasswordErrors.noUppercase);
      _pswdReqFulfilled = errors.isEmpty;
    });
  }

  @override
  void dispose() {
    _pswdController.dispose();
    _pswdRepeatController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext ctx) => Scaffold(
      appBar: AppBar(
        title: Text(ctx.translate('new_password_title')),
      ),
      body: LoadingOverlay(
        isLoading: _isLoading,
        child: Container(
          padding: EdgeInsets.all(20),
          child: Form(
            key: _formKey,
            child: ListView(
              children: [
                Text(ctx.translate('new_password_body'), style: TextStyle(fontSize: 16)),
                ColDivider(),
                TextFormField(
                  validator: _validatePswd,
                  onChanged: _onPasswordChanged,
                  decoration: InputDecoration(labelText: ctx.translate('new_password_label')),
                  controller: _pswdController,
                  obscureText: true,
                ),
                TextFormField(
                  validator: _validatePswdRepeat,
                  decoration: InputDecoration(labelText: ctx.translate('new_password_repeat_label')),
                  controller: _pswdRepeatController,
                  obscureText: true,
                ),
                _mode == NewPasswordMode.change
                    ? SizedBox()
                    : TextFormField(
                        validator: _validateVerificationCode,
                        decoration: InputDecoration(labelText: ctx.translate('email')),
                        controller: _mailController,
                      ),
                _mode == NewPasswordMode.change
                    ? SizedBox()
                    : TextFormField(
                        validator: _validateVerificationCode,
                        decoration: InputDecoration(labelText: ctx.translate('verification_code')),
                        controller: _codeController,
                      ),
                ColDivider(),
                Container(
                  padding: EdgeInsets.all(8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text('${ctx.translate('new_password_constraints')}:', style: TextStyle(fontSize: 12)),
                      SizedBox(
                        height: 4,
                      ),
                      _PasswordReqLine(ctx.translate('pswd_req_length'), _pswdHasLength),
                      _PasswordReqLine(ctx.translate('pswd_req_digits'), _pswdHasDigits),
                      _PasswordReqLine(ctx.translate('pswd_req_uppercase'), _pswdHasUppercase),
                      _PasswordReqLine(ctx.translate('pswd_req_lowercase'), _pswdHasLowercase),
                    ],
                  ),
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(10), border: Border.all(color: Colors.grey)),
                ),
                ColDivider(),
                PrimaryButton(onPressed: _onNewPassword, text: ctx.translate('new_passwort_button')),
              ],
            ),
          ),
        ),
      ));
}

enum NewPasswordMode { reset, change }

class _PasswordReqLine extends StatelessWidget {
  final String _text;
  final IconData _icon;
  final MaterialColor _color;

  _PasswordReqLine(
    this._text,
    bool isValid, {
    Key key,
  })  : _icon = isValid ? Icons.check : Icons.circle,
        _color = isValid ? Colors.green : Colors.grey,
        super(key: key);

  @override
  Widget build(BuildContext ctx) => Row(children: [
        Icon(_icon, color: _color, size: 8),
        SizedBox(
          width: 6,
        ),
        Text(
          _text,
          style: TextStyle(fontSize: 12, color: _color),
        )
      ]);
}

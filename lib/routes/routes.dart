import 'package:toroxcheck/pages/check/check_1.dart';
import 'package:toroxcheck/pages/check/check_2.dart';
import 'package:toroxcheck/pages/check/check_device.dart';
import 'package:toroxcheck/pages/check/check_list.dart';
import 'package:toroxcheck/pages/check/check_start.dart';
import 'package:toroxcheck/pages/check/check_steps_list.dart';
import 'package:toroxcheck/pages/coupling/coupling_activate_bluetooth.dart';
import 'package:toroxcheck/pages/coupling/coupling_connecting.dart';
import 'package:toroxcheck/pages/coupling/coupling_device_found.dart';
import 'package:toroxcheck/pages/coupling/coupling_device_not_found.dart';
import 'package:toroxcheck/pages/coupling/coupling_start.dart';
import 'package:toroxcheck/pages/dashboard/dashboard.dart';
import 'package:toroxcheck/pages/dashboard/settings.dart';
import 'package:toroxcheck/pages/infos/imprint.dart';
import 'package:toroxcheck/pages/infos/privacy.dart';
import 'package:toroxcheck/pages/infos/terms_of_use.dart';
import 'package:toroxcheck/pages/login/login.dart';
import 'package:toroxcheck/pages/login/new_password.dart';
import 'package:toroxcheck/pages/password_reset/password_forgot.dart';
import 'package:toroxcheck/pages/selftest/selftest.dart';

Object appRoutes() => {
      Routes.login: (ctx) => LoginPage(),
      Routes.imprint: (ctx) => ImprintPage(),
      Routes.privacy: (ctx) => PrivacyPage(),
      Routes.settings: (ctx) => SettingsPage(),
      Routes.checkOne: (ctx) => CheckOnePage(),
      Routes.checkTwo: (ctx) => CheckTwoPage(),
      Routes.selftest: (ctx) => SelftestPage(),
      Routes.dashboard: (ctx) => DashboardPage(),
      Routes.checkList: (ctx) => CheckListPage(),
      Routes.checkSteps: (ctx) => CheckStepsListPage(),
      Routes.checkStart: (ctx) => CheckStartPage(),
      Routes.termsOfUse: (ctx) => TermsOfUsePage(),
      Routes.checkDevice: (ctx) => CheckDevicePage(),
      Routes.couplingStart: (ctx) => CouplingStartPage(),
      Routes.forgotPassword: (ctx) => PasswordForgotPage(),
      Routes.changePassword: (ctx) => NewPasswordPage(NewPasswordMode.change),
      Routes.resetPassword: (ctx) => NewPasswordPage(NewPasswordMode.reset),
      Routes.couplingConnecting: (ctx) => CouplingConnectingPage(),
      Routes.couplingDeviceFound: (ctx) => CouplingDeviceFoundPage(),
      Routes.couplingDeviceNotFound: (ctx) => CouplingDeviceNotFoundPage(),
      Routes.couplingActivateBluetooth: (ctx) => CouplingActivateBluetoothPage(),
    };

class Routes {
  // Checks
  static const checkOne = '/checkOne';
  static const checkTwo = '/checkTwo';
  static const checkStart = '/checkStart';
  static const checkSteps = '/checkSteps';

  // General
  static const login = '/login';
  static const imprint = '/imprint';
  static const privacy = '/privacy';
  static const settings = '/settings';
  static const selftest = '/selftest';
  static const checkList = '/check_list';
  static const dashboard = '/dashboard';
  static const termsOfUse = '/terms_of_use';
  static const checkDevice = '/check_device';
  static const resetPassword = '/reset_password';
  static const couplingStart = '/coupling_start';
  static const forgotPassword = '/forgot_password';
  static const changePassword = '/new_password';
  static const couplingConnecting = '/coupling_connecting';
  static const couplingDeviceFound = '/coupling_device_found';
  static const couplingDeviceNotFound = '/coupling_device_not_found';
  static const couplingActivateBluetooth = '/coupling_activate_bluetooth';
}
